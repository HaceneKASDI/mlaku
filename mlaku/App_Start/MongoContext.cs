﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using System.Configuration;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;

namespace mlaku.App_Start
{
    public class MongoContext
    {
        MongoClient _client;
        MongoServer _mongoServer;

        public MongoDatabase _mongoDatabase;
        public MongoContext()
        {
            // Reading credentials from Web.config file   
            var MongoDatabaseName = ConfigurationManager.AppSettings["MongoDatabaseName"]; //mlaku
            var MongoUsername = ConfigurationManager.AppSettings["MongoUsername"]; //
            var MongoPassword = ConfigurationManager.AppSettings["MongoPassword"]; // 
            var MongoPort = ConfigurationManager.AppSettings["MongoPort"];  //27017  
            var MongoHost = ConfigurationManager.AppSettings["MongoHost"];  //localhost  

            _client = new MongoClient(MongoUrl.Create(MongoHost+MongoPort));
            _mongoServer = _client.GetServer();
            _mongoDatabase = _mongoServer.GetDatabase(MongoDatabaseName);

        }
    }
}