﻿using mlaku.App_Start;
using mlaku.Models;
using mlaku.Models.BAL;
using mlaku.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mlaku.Controllers
{
    public class NewsController : Controller
    {
        INewsService<NewsEntity> _newService;

        public NewsController()
        {
            _newService = new NewsService<NewsEntity>();
        }

        // GET: News
        public ActionResult Index()
        {
            //MainUser user = new MainUser { Username = "AZZ", Year = "4444" };
            //var res = _mongoContext._mongoDatabase.GetCollection<MainUser>("mainuser").Save(user);

            NewsEntity news = new NewsEntity
            {
                Author = "Hacene",
                ImageUrl = "//iuiuiou.com",
                DateEdition = "2018/10/20",
                ContentNews = "Bla bla bla bla bla bla bla bla bla bla bla",
                Title = "First news in mongoDB"

            };

            _newService.saveEntity(news);


            List <NewsEntity> collection = (List<NewsEntity>)_newService.GetAll();
            return View();
        }

        public JsonResult GetAllNews()
        {
            List<MainUser> collection = (List<MainUser>)_newService.GetAll();
            return null;
        }
    }
}