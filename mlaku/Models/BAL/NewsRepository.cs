﻿using mlaku.Models.DAL;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mlaku.Models.BAL
{
    public class NewsRepository<T> : Repository<T>, INewsRepository<T> where T : class
    {
        public MongoCollection<T> _mongoCollectionUSER
        {
            get { return _mongoCollection as MongoCollection<T>; }
        }

        public NewsRepository(MongoCollection<T> mongoCollection) : base(mongoCollection)
        {
        }
    }
}