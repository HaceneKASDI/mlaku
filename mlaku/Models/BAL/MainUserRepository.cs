﻿using mlaku.Models.DAL;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mlaku.Models.BAL
{
    // This Data access Object (Repository) extends geniric Repository and 
    // implements the interface IMainUserRepository

    public class MainUserRepository<T> : Repository<T>, IMainUserRepository<T> where T : class
    {
        public MongoCollection<T> _mongoCollectionUSER
        {
            get { return _mongoCollection as MongoCollection<T>;  }
        }

        public MainUserRepository(MongoCollection<T> mongoCollection) : base(mongoCollection)
        {
        }

    }
}