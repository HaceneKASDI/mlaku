﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Generic repository pattern used to define common database CRUD operations for MLAKU Application

namespace mlaku.Models.DAL
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(object Id);
        void AddToMongodb(T entity);
        void UpdateOnMongodb(T entity);
        void RemoveFromMongodbByID(object Id);
        void RemoveFromMongodb(T entity);
    }
}
