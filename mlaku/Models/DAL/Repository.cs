﻿using mlaku.App_Start;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mlaku.Models.DAL
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly MongoCollection _mongoCollection;

        public Repository(MongoCollection mongoCollection)
        {
            _mongoCollection = mongoCollection;
        }

        public IEnumerable<T> GetAll()
        {
            return _mongoCollection.FindAllAs<T>().ToList<T>();
        }
        public T GetById(object Id)
        {
            return null;
        }
        public void AddToMongodb(T entity)
        {
            _mongoCollection.Save<T>(entity);
        }

        public void UpdateOnMongodb(T entity)
        { }
        public void RemoveFromMongodbByID(object Id)
        { }
        public void RemoveFromMongodb(T entity)
        { }
    }
}