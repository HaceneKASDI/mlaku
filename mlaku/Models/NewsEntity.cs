﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mlaku.Models
{
    public class NewsEntity
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("author")]
        public string Author;

        [BsonElement("dateEdition")]
        public string DateEdition;

        [BsonElement("title")]
        public string Title;

        [BsonElement("contentNews")]
        public string ContentNews;

        [BsonElement("imageUrl")]
        public string ImageUrl;
        
    }
}