﻿using mlaku.App_Start;
using mlaku.Models;
using mlaku.Models.BAL;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mlaku.Services
{
    public class NewsService<T> : INewsService<T> where T : class
    {
        private MongoContext                    _mongoContext;
        private NewsRepository<NewsEntity>      _newsDAO;
        private MongoCollection<NewsEntity>     _mongoCollection;

        public NewsService()
        {
            _mongoContext       = new MongoContext();
            _mongoCollection    = _mongoContext._mongoDatabase.GetCollection<NewsEntity>("news");
            _newsDAO            = new NewsRepository<NewsEntity>(_mongoCollection);
        }

        public IEnumerable<T> GetAll()
        {
            return (IEnumerable<T>)_newsDAO.GetAll();
        }

        public T GetById(object Id)
        {
            throw new NotImplementedException();
        }

        public void Remove(T entity)
        {
            throw new NotImplementedException();
        }

        public void RemoveByID(object Id)
        {
            throw new NotImplementedException();
        }

        public void saveEntity(T entity)
        {
            _newsDAO.AddToMongodb(entity as NewsEntity);
        }

        public void UpdateEntity(T entity)
        {
            throw new NotImplementedException();
        }
    }
}