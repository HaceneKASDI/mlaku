﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mlaku.Services
{
    public interface IGenericService<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(object Id);
        void saveEntity(T entity);
        void UpdateEntity(T entity);
        void RemoveByID(object Id);
        void Remove(T entity);
    }
}
