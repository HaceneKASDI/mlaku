﻿// This is the root of angularJS controllers used in MLAKU application
var MainController = angular.module('MainController', ['ngRoute']);

// Adding the controllers used in SPA (MLAKU Single Page Application )

MainController.controller('NewsController', NewsController);
